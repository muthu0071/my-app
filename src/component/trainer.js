import React from 'react';

const trainer = {
    name:'',
    age:'',
    description:'',
}

class Trainer extends React.Component {
      
    constructor(props) {
        super(props);
        this.state = {trainer}
        console.log("-->this",this)
        this.handleChange = this.handleChange.bind(this.trainer);
        this.handleSubmit = this.handleSubmit.bind(this.trainer);
      }
    
      handleChange = (event) => {
        const field = event.target.name;
        const value = event.target.value
        console.log("--;.field", field, value)
       console.log("-->",this.props)
       this.setState({ [field]: value },() =>{
         console.log("cbvall>",this.state.trainer)
       });
      }
      handleSubmit = event => {
        console.log("--> submit event", this.state.trainer)
        alert('A name was submitted: ' + this.state.trainer.name);
        event.preventDefault();
      }
   handleChangeName = event =>{
     console.log("--> ava", event.target.value)
     this.setState({
       name:event.target.value
     })
   }
render(){
    return (
        <form>
        <label>
          Name:
          <input onChange={this.handleChange} type="text" name = "name" value={this.state.trainer.name}  />
        </label>
        <label>
          Age:
          <input type="text" name = "age" value={this.state.trainer.age} onChange={this.handleChange} />
        </label>
        <label>
        description:
          <input type="text" name = "description" value={this.state.trainer.description} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" onClick={this.handleSubmit}/>
      </form>
        )
}
}

export default Trainer